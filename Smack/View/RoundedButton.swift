//
//  RoundedButton.swift
//  Smack
//
//  Created by Zachary Smouse on 1/25/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

@IBDesignable

class RoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        
        didSet {
            
            self.layer.cornerRadius = cornerRadius
            
        }
        
    }
    
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        
        self.layer.cornerRadius = cornerRadius
        
    }

}
