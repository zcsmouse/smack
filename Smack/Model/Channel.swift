//
//  Channel.swift
//  Smack
//
//  Created by Zachary Smouse on 1/28/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation

struct Channel : Decodable {
    
    public private(set) var channelTitle: String!
    public private(set) var channelDescription: String!
    public private(set) var id: String!
    
}
